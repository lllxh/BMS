package bms.business;

import bms.tools.DBUtil;
import bms.tools.User;

import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * @author lllxh
 */
public class Business {
    private Scanner input = new Scanner(System.in);
    User user;
    DBUtil dbUtil = new DBUtil();
    private String cardId,cardPwd;

    //定义构造方法，传入主类的user、dbUtil
    public Business(User user,DBUtil dbUtil){
        this.user = user;
        this.dbUtil = dbUtil;
    }
    public void initBusiness(){
        int key5;
        System.out.println("欢迎进入用户模式");
        while (true){
            System.out.print("请输入银行卡号：");
            cardId = input.next();
            System.out.print("请输入银行卡密码：");
            cardPwd = input.next();
            if (checkPwd(cardId,cardPwd)){
                System.out.println("登录成功！");
                while (true){
                    user = (User) dbUtil.getUsers().get(cardId);
                    System.out.println("请选择您的业务：");
                    enterBusiness();
                    key5 = input.nextInt();
                    switch (key5){
                        case 1:
                            deposit(user);
                            break;
                        case 2:
                            withdrawal(user);
                            break;
                        case 3:
                            balance(user);
                            break;
                        default:
                    }
                    if (key5 == 0) break;
                }
                break;
            } else {
                System.out.println("银行卡号或密码错误，请重新输入！");
            }
        }
    }

    public void deposit(User user){
        double temp1,temp2;
        String answer = "no";
        System.out.println("当前处于：银行管理系统➤用户模式➤存款");
        do {
            System.out.print("请输入存款的金额：");
            temp1 = input.nextDouble();
            if (temp1>0){
                temp2 = user.getAccount();
                temp2 += temp1;
                user.setAccount(temp2);
                System.out.println("存款成功！");
                System.out.println("您当前的余额是："+user.getAccount());
                System.out.println("继续存款，请输入'yes',返回上级菜单,请输入'no'");
                answer = input.next();
            }else {
                System.out.println("输入有误，不能输入小于0的数！");
            }
        } while (answer.equals("yes"));

    }

    public void withdrawal(User user){
        double temp1,temp2;
        String answer = "no";
        System.out.println("当前处于：银行管理系统➤用户模式➤取款");
        do {
            System.out.print("请输入取款的金额：");
            temp1 = input.nextDouble();
            if (temp1<=user.getAccount() && temp1>0){
                temp2 = user.getAccount();
                temp2 -= temp1;
                user.setAccount(temp2);
                System.out.println("取款成功！");
                System.out.println("您当前的余额是："+user.getAccount());
                System.out.println("继续取款，请输入'yes',返回上级菜单,请输入'no'");
                answer = input.next();
            }
            else if (temp1<=0){
                System.out.println("别想歪脑筋，请正常输入!");
            }
            else {
                System.out.println("余额不足，取款失败！");
            }
        } while (answer.equals("yes"));

    }

    public void balance(User user){
        String answer = "no";
        System.out.println("当前处于：银行管理系统➤用户模式➤余额查询");
        System.out.println("您当前的余额是："+user.getAccount());
        System.out.println("返回上级菜单,请输入'no'");
        answer = input.next();
    }

    public boolean checkPwd(String cardId,String cardPwd){
        boolean flag = false;
        //遍历
        Iterator iterator = dbUtil.getUsers().entrySet().iterator();
        while(iterator.hasNext()){
            //用entry set接收遍历的用户对象
            Map.Entry<String,User> entry = (Map.Entry<String,User>) iterator.next();
            if (cardId.equals(entry.getKey())){
                if (cardPwd.equals(entry.getValue().getCardPwd())){
                    flag = true;
                }
            }
        }
        return flag;
    }

    public void enterBusiness(){
        System.out.println("******************************");
        System.out.println("1.存款");
        System.out.println("2.取款");
        System.out.println("3.余额查询");
        System.out.println("0.退出");
        System.out.println("******************************");
        System.out.print("请选择：");
    }
}
