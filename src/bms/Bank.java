package bms;

import bms.business.Business;
import bms.root.Root;
import bms.tools.DBUtil;
import bms.tools.User;

import java.util.Scanner;

/**
 * @author lllxh
 */
public class Bank {
    Scanner input = new Scanner(System.in);
    //主类定义User DBUtil的对象，保证整个程序的一致性
    User user;
    DBUtil dbUtil = new DBUtil();

    public Bank(){
        start();
    }

    public void start(){
        //实例管理员模式类和用户模式类，传入user，dbUtil
        Root root = new Root(user,dbUtil);
        Business business = new Business(user,dbUtil);
        int key1;
        while (true){
            enterBank();
            try {
                //key1接收用户输入的选择
                key1 = input.nextInt();
                if (key1 == 1){
                    business.initBusiness();
                }
                else if (key1 == 2) {
                    root.initRoot();
                }

                else if (key1 == 0) {
                    System.out.println("\n感谢您的使用，再见！");
                    System.out.println("ฅ'ω'ฅ♪");
                    break;}
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void enterBank(){
        System.out.println("系统启动中，请稍后......");
        System.out.println("欢迎进入银行管理系，请选择要进入的模式：");
        System.out.println("----------------------------------------------");
        System.out.println("1.用户模式");
        System.out.println("2.管理员模式");
        System.out.println("0.退出系统");
        System.out.println("----------------------------------------------");
        System.out.print("请选择：");
    }


}



