package bms.tools;

/**
 * @author lllxh
 */
public class User {
    //定义用户信息变量
    public String cardId;
    public String cardPwd;
    public String userName;
    public String  call;
    public double account;

    //设置getter和setter
    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardPwd() {
        return cardPwd;
    }

    public void setCardPwd(String cardPwd) {
        this.cardPwd = cardPwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public double getAccount() {
        return account;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    //定义构造方法
    public User(String cardId,String cardPwd,String userName,String call,double account){
        setCardId(cardId);
        setCardPwd(cardPwd);
        setUserName(userName);
        setCall(call);
        setAccount(account);
    }

}
