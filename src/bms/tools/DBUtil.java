package bms.tools;

import java.util.HashMap;
import java.util.Iterator;

/**
 * @author lllxh
 */
public class DBUtil {
    //定义HashMap
    HashMap<String,User> users = new HashMap();

    public DBUtil(){ }

    //获取HashMap
    public HashMap getUsers(){
        return users;
    }

    //添加用户
    public void addUser(String cardId,User user){
        users.put(cardId,user);
    }

    //获取所有用户信息
    public void getAllUserInfo(){
        Iterator iterator = users.keySet().iterator();
        System.out.println("************************************用户列表****************************************");
        while (iterator.hasNext()){
            Object key = iterator.next();
            User value1 = users.get(key);
            String format = "银行卡号：%-8s银行卡密码：%-8s账户名：%-8s电话号码：%-8s账户余额：%-8f\n";
            System.out.printf(format,value1.getCardId(),value1.getCardPwd(),
                    value1.getUserName(),value1.getCall(),value1.getAccount());
        }
    }

    //获取单个用户信息
    public void getSingleUser(Object key){
        User value2 = users.get(key);
        System.out.println("*******用户信息*******");
        System.out.println("银行卡号："+key);
        System.out.println("银行卡密码："+value2.getCardPwd());
        System.out.println("账户名："+value2.getUserName());
        System.out.println("电话号码："+value2.getCall());
        System.out.println("账户余额："+value2.getAccount());
        System.out.println("*********************");
    }

    //删除用户
    public void delete(Object key){
        users.remove(key);
    }


}
