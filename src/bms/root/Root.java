package bms.root;

import bms.tools.DBUtil;
import bms.tools.User;

import java.util.Scanner;

/**
 * @author lllxh
 */
public class Root {
    private Scanner input = new Scanner(System.in);
    User user;
    DBUtil dbUtil = new DBUtil();
    final String passWd = "654321";
    final String rootName = "root";
    private String inputPassWd,inputRootName;

    public Root(User user,DBUtil dbUtil){
        this.user = user;
        this.dbUtil = dbUtil;
    }

    public void initRoot(){
        int key;
        System.out.println("欢迎进入管理员模式");
        while (true) {
            System.out.print("请输入管理员账号：");
            inputRootName = input.next();
            System.out.print("请输入账号密码：");
            inputPassWd = input.next();
            if (checkPwd(inputRootName, inputPassWd)) {
                System.out.println("登录成功！");
                while (true) {
                    enterRoot();
                    key = input.nextInt();
                    switch (key) {
                        case 1:
                            createUser();
                            break;
                        case 2:
                            deleteUser();
                            break;
                        case 3:
                            searchUser();
                            break;
                        default:
                    }
                    if (key == 0) break;
                }
                break;
            } else {
                System.out.println("账号或密码错误，请重新输入！");
            }
        }
    }

    public void enterRoot(){
        System.out.println("******************************");
        System.out.println("1.添加用户");
        System.out.println("2.删除用户");
        System.out.println("3.查询用户");
        System.out.println("0.退出");
        System.out.println("******************************");
        System.out.print("请选择：");
    }

    public void createUser(){
        String answer = "no";
        System.out.println("\n当前处于：银行管理系统➤管理员模式➤添加用户\n");
        do {
            System.out.println("请设置银行卡号：");
            String cardId = input.next();
            System.out.println("请设置银行卡密码：");
            String cardPwd = input.next();
            System.out.println("请设置账户名：");
            String userName = input.next();
            System.out.println("请设置手机号码：");
            String call = input.next();
            System.out.println("请设置存款：");
            double account = input.nextDouble();
            user = new User(cardId,cardPwd,userName,call,account);
            dbUtil.addUser(user.getCardId(), user);
            System.out.println("用户创建成功，输入'yes'继续创建，或输入'no'返回上一级");
            answer = input.next();
        } while (answer.equals("yes"));


    }

    public void deleteUser(){
        String answer = "no";
        System.out.println("\n当前处于：银行管理系统➤管理员模式➤删除用户");
        do {
            System.out.println("请输入要删除用户的银行卡号");
            String key2 = input.next();
            dbUtil.delete(key2);
            System.out.println("用户已删除，当前用户列表如下\n");
            dbUtil.getAllUserInfo();
            System.out.println("输入'yes'继续删除，或输入'no'返回上一级");
            answer = input.next();
        } while (answer.equals("yes"));

    }

    public void searchUser(){
        String answer = "no";
        int choice;
        System.out.println("\n当前处于：银行管理系统➤管理员模式➤查询用户\n");
        do {
        System.out.println("******************************");
        System.out.println("1.查询单个用户");
        System.out.println("2.查看所有用户");
        System.out.println("******************************");
        choice = input.nextInt();
        if (choice==1) {
            System.out.println("请输入要查询用户的银行卡号");
            String key3 = input.next();
            dbUtil.getSingleUser(key3);
        }
        else if (choice == 2){
            dbUtil.getAllUserInfo();
        }
        System.out.println("查询完成，输入'yes'继续查询，或输入'no'返回上一级");
        answer = input.next();
        }while (answer.equals("yes"));
    }

    public boolean checkPwd(String inputRootName,String inputPassWd){
        boolean flag = false;
        if (inputRootName.equals(rootName)){
            if (inputPassWd.equals(passWd)){
                flag = true;
            }
        }
        return flag;
    }
}
